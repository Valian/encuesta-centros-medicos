<?php
    session_start();
    if(!isset($_SESSION)) {
      ?>
      <script>
          window.location.href="./index.html"
      </script>
      <?php
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Estadisticas</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="row mt-5">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
        <table class="table">
        <thead>
            <tr>
                <th>#ID</th>
                <th>nombre del medico</th>
                <th>Cantidad de atención</th>
                <th>Puntaje</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Will Smith</td>
                <td>1000</td>
                <td>100</td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Pedro Rodriguez</td>
                <td>5</td>
                <td>6</td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>Oscar soto</td>
                <td>1</td>
                <td>1</td>
            </tr>
            <tr>
                <th scope="row">4</th>
                <td>Jacob Nicolas Troncoso</td>
                <td>20</td>
                <td>60</td>
            </tr>
            <tr>
                <th scope="row">5</th>
                <td>Path Adams</td>
                <td>10000</td>
                <td>1000</td>
            </tr>
        </tbody>
    </table>
        </div>
        <div class="col-sm-3"></div>
    </div>
</body>
</html>